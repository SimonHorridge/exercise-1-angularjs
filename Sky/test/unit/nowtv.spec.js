describe('Sanity check', function () {

    it('nowtv.com contains nowtv', function () {
        expect('nowtv.com').toContain('nowtv');
    });

});


describe('controller instantiation', function () {
    beforeEach(module('app'));
    var $controller;
    beforeEach(inject(function (_$controller_) {
        // The injector unwraps the underscores (_) from around the parameter names when matching
        $controller = _$controller_;
    }));

    it('test model exists', function () {
        var $scope = {};
        var controller = $controller('MainCtrl', { $scope: $scope });
        expect(controller).not.toBeNull();
        expect($scope.model).not.toBeNull();
    });
});

describe('movieHelper functions', function () {
    var $controller, data, movieHelper;

    beforeEach(module('app'));

    beforeEach(inject(function (_$controller_, _data_, _movieHelper_) {
        $controller = _$controller_;
        data = _data_;
        movieHelper = _movieHelper_;
    }));

    it('test data', function (done) {
        var $scope = {}, allMovies = [];
        var controller = $controller('MainCtrl', { $scope: $scope, data: data });

        data.getJson().then(
            function (d) { allMovies = d.movies }
        ).finally(done())

        expect(allMovies.length).toBe(120)
    });

    it('test sort', function () {
        var movies = [
            { title: "C" },
            { title: "A" },
            { title: "b" },
        ]
        var sorted = movieHelper.sort(movies);

        expect(sorted[0].title).toBe("A");
        expect(sorted[1].title).toBe("b");
        expect(sorted[2].title).toBe("C");
    });

    it('test filter', function () {

        var movies = [
            { title: "Movie1" },
            { title: "MOVIE2" },
            { title: "movie3" },
            { title: "Another Film" },
        ]

        var filtered = movieHelper.filter(movies, "", 20);
        expect(filtered.length).toBe(4);

        var filtered = movieHelper.filter(movies, "m", 20);
        expect(filtered.length).toBe(0);

        var filtered = movieHelper.filter(movies, "mo", 20);
        expect(filtered.length).toBe(0);

        var filtered = movieHelper.filter(movies, "mov", 20);
        expect(filtered.length).toBe(3);

        var filtered = movieHelper.filter(movies, "not in the list", 20);
        expect(filtered.length).toBe(0);

    })


    it('test filter maxLength', function () {
        var movies = [
            { title: "Movie1" },
            { title: "MOVIE2" },
            { title: "movie3" },
            { title: "Another Film" },
        ]

        var filtered = movieHelper.filter(movies, "movie", 2);
        expect(filtered.length).toBe(2);

        expect(filtered[0].title).toBe("Movie1");
        expect(filtered[1].title).toBe("MOVIE2");
    })

    it('test getActors', function () {
        var movie = {
            "actors": {
                "list": [{
                    "name": "James Spader"
                }, {
                    "name": "Danny Aiello"
                }, {
                    "name": "Eric Stoltz"
                }]
            },
        }

        expect(movieHelper.getActors(movie)).toBe('James Spader, Danny Aiello, Eric Stoltz')
    });

    it("Test x Of y message - value for x", function () {
        var x = 1, y = 20;
        expect(movieHelper.getXofYMessage(x, y)).toBe("Matched " + x + " of " + y + " movies total")
    });

    it("Test x Of y message - x = 0", function () {
        var x = 0, y = 20;
        expect(movieHelper.getXofYMessage(x, y)).toBe("No matching items")
    })
});
