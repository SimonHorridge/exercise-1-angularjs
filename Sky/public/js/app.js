(function () {

    var app = angular.module('app', [])
    var defaultFilterMessage = 'Enter at least three characters to begin search';
    var maxNumberOfResults = 20;
    var searchMinFilterLength = 3;
    app.controller('MainCtrl', ['$scope', 'data', 'movieHelper', function ($scope, data, movieHelper) {

        var model = $scope.model = {
            filter: "",
            filterMessage: "",
            allMovies: [],
            movies: [],
            dataLoaded: false,
            getActors: movieHelper.getActors,
            getXofYMessage: movieHelper.getXofYMessage
        };

        data.getJson().then(function (data) {
            model.dataLoaded = true;
            model.allMovies = movieHelper.sort(data.movies);
            $scope.$watch(
                "model.filter", function () { onFilterChange(model) }
            )
        });

        function onFilterChange(model) {
            if (model.dataLoaded) {
                var filterLength = model.filter.length
                model.filterMessage = (filterLength > 0 && filterLength < searchMinFilterLength) ? defaultFilterMessage : "";
                model.movies = movieHelper.filter(model.allMovies, model.filter, maxNumberOfResults);
            }
        }

    }]);

    app.factory('movieHelper', function () {

        return {
            sort: function (movies) {
                return movies.sort(function (a, b) {
                    var titleA = a.title.toUpperCase();
                    var titleB = b.title.toUpperCase();

                    return (titleA < titleB) ? -1 : (titleA > titleB) ? 1 : 0;
                });
            },
            filter: function (movies, filterString, maxLength) {
                var rtn = [];

                if (filterString.length > 0 && filterString.length < 3) {
                    return rtn
                }

                var pattern = new RegExp(filterString, "i")
                var resultCount = 0;
                var i = 0;
                for (i = 0; i < movies.length && rtn.length < maxLength; i++) {
                    var item = movies[i];
                    if (filterString=="" || item.title.search(pattern) !== -1) {
                        rtn.push(item)
                    }
                }
                return rtn;
            },
            getActors: function (movie) {
                return movie.actors.list.reduce(function (previousValue, currentValue) {
                    var pre = previousValue
                    return previousValue ? previousValue + ", " + currentValue.name : currentValue.name
                }, "")
            },
            getXofYMessage: function (x, y) {
                if (x == 0) {
                    return "No matching items"
                }
                return "Matched " + x + " of " + y + " movies total"
            }
        }
    })

    app.factory('data', ['$http', function ($http) {

        return {

            getJson: function () {

                return $http.get('data.json')
                    .then(
                        function (response) {
                            return response.data;
                        },
                        function (httpError) {
                            throw httpError.status + " : " + httpError.data;
                        });

            }

        }

    }]);

})();
